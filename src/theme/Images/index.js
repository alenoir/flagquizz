import Back from './Back';
import Hint from './Hint';
import Next from './Next';
import Play from './Play';

export default {
  Back,
  Hint,
  Next,
  Play,
};
