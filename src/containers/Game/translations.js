import { defineMessages } from 'react-intl';

export default defineMessages({
  gameButtonSkip: {
    id: 'Game.button.skip.title',
    defaultMessage: 'Passer',
    description: 'Game.button.skip.title',
  },

  alertTitle: {
    id: 'Game.alert.title',
    defaultMessage: 'Besoin d\'aide',
    description: 'Game.alert.title',
  },

  alertDescription: {
    id: 'Game.alert.description',
    defaultMessage: 'Un indice s\'affichera après cette pub',
    description: 'Game.alert.description',
  },

  alertButtonOk: {
    id: 'Game.alert.button.ok',
    defaultMessage: 'OK',
    description: 'Game.alert.button.ok',
  },

  alertButtonCancel: {
    id: 'Game.alert.button.cancel',
    defaultMessage: 'Non merci',
    description: 'Game.alert.button.cancel',
  },
});
