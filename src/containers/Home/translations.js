import { defineMessages } from 'react-intl';

export default defineMessages({

  homeTitle: {
    id: 'Home.title',
    defaultMessage: 'Flag',
    description: 'Home.title',
  },
  homeSubtitle: {
    id: 'Home.subtitle',
    defaultMessage: 'QUIZ',
    description: 'Home.subtitle',
  },
  homeButton: {
    id: 'Home.button.title',
    defaultMessage: 'Play',
    description: 'Home.button.title',
  },

});
