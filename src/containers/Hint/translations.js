import { defineMessages } from 'react-intl';

export default defineMessages({
  hintTitle: {
    id: 'Hint.title',
    defaultMessage: 'Le pays que vous essayez de deviner est dans cette zone',
    description: 'Hint.title',
  },
});
