export const SearchCustomerTypes = {
  personal: 'SEARCH_TYPE_PERSONAL',
  professional: 'SEARCH_TYPE_PROFESSIONAL',
};

export const SearchVehicleTypes = {
  new: 'SEARCH_VEHICLE_TYPE_NEW',
  used: 'SEARCH_VEHICLE_TYPE_USED',
};

export const SearchFinanceTypes = {
  LOA: 'SEARCH_FINANCE_TYPE_LOA',
  credit: 'SEARCH_FINANCE_TYPE_CREDIT',
  creditBail: 'SEARCH_FINANCE_TYPE_BAIL_CREDIT',
};

export const SearchVehicleCategories = {
  personal: 'SEARCH_VEHICLE_CATEGORY_PERSONAL',
  professional: 'SEARCH_VEHICLE_CATEGORY_PROFESSIONAL',
};

export const SearchResidualValueTypes = {
  percent: 'percent',
  max: 'max',
};

export const InsuranceCategories = {
  perteFi: 'INSURANCE_CATEGORY_PERTE_FI',
  vie: 'INSURANCE_CATEGORY_VIE',
  entretien: 'INSURANCE_CATEGORY_ENTRETIEN',
  garantie: 'INSURANCE_CATEGORY_GARANTIE',
};

export const SearchFields = {
  customerType: 'customerType',
  financeType: 'financeType',
  vehicleType: 'vehicleType',
  registrationDate: 'registrationDate',
  mileage: 'mileage',
  brand: 'brand',
  model: 'model',
  modelYear: 'modelYear',
  energy: 'energy',
  version: 'version',
  finishes: 'finishes',
  vehicleCategory: 'vehicleCategory',
  capital: 'capital',
  duration: 'duration',
  annualMileage: 'annualMileage',
  catalogPriceTTC: 'catalogPriceTTC',
  priceTTC: 'priceTTC',
  contributionTTC: 'contributionTTC',
  charges: 'charges',
  optionsTTC: 'optionsTTC',
  facturedPriceTTC: 'facturedPriceTTC',
  firstRentTTC: 'firstRentTTC',
  residualValue: 'residualValue',
  residualValueTTC: 'residualValueTTC',
  residualValueType: 'residualValueType',
  senderEmail: 'senderEmail',
  toEmails: 'toEmails',
};

export const FileStatus = {
  PENDING: 'FILE_STATUS_PENDING',
  IN_PROGRESS: 'FILE_STATUS_IN_PROGRESS',
  ERROR: 'FILE_STATUS_ERROR',
  FINISH: 'FILE_STATUS_FINISH',
  LIVE: 'FILE_STATUS_LIVE',
};

export const FileType = {
  BAREME: 'FILE_TYPE_BAREME',
  ARGUS: 'FILE_TYPE_ARGUS',
  INSURANCE: 'FILE_TYPE_INSURANCE',
  BONUS_MALUS: 'FILE_TYPE_BONUS_MALUS',
  TVS: 'FILE_TYPE_TVS',
  TVS_AIR: 'FILE_TYPE_TVS_AIR',
  FUEL: 'FILE_TYPE_FUEL',
  AND: 'FILE_TYPE_AND',
  USER: 'FILE_TYPE_USER',
  DEALER: 'FILE_TYPE_DEALER',
  VR: 'FILE_TYPE_VR',
};
